﻿using Data;

namespace DataAccess.Repository.Word
{
    public class WordRepository : Repository<Domain.Entities.Word>, IWordRepository, IRepositoryIdentifier
    {
        public WordRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

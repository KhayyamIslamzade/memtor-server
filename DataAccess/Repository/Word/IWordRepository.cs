﻿namespace DataAccess.Repository.Word
{
    public interface IWordRepository : IRepository<Domain.Entities.Word>
    {
    }
}

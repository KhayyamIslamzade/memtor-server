﻿using Data;

namespace DataAccess.Repository.Session
{
    public class SessionRepository : Repository<Domain.Entities.Session>, ISessionRepository, IRepositoryIdentifier
    {
        public SessionRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

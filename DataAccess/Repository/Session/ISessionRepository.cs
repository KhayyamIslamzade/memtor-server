﻿namespace DataAccess.Repository.Session
{
    public interface ISessionRepository : IRepository<Domain.Entities.Session>
    {
    }
}

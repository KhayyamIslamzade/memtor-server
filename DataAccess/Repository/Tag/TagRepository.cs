﻿using Data;

namespace DataAccess.Repository.Tag
{
    public class TagRepository : Repository<Domain.Entities.Tag>, ITagRepository, IRepositoryIdentifier
    {
        public TagRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

﻿namespace DataAccess.Repository.Tag
{
    public interface ITagRepository : IRepository<Domain.Entities.Tag>
    {
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository.User
{
    public interface IUserRepository
    {
        Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName);

        Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName,
            params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties);

        Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName, params string[] includeProperties);
        Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId);

        Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId,
            params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties);

        Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId, params string[] includeProperties);

        Task<Domain.Entities.Identity.User> GetUserByEmailAsync(string email);
        IQueryable<Domain.Entities.Identity.User> FindBy(
            Expression<Func<Domain.Entities.Identity.User, bool>> predicate);




        IQueryable<Domain.Entities.Identity.User> FindBy(
            Expression<Func<Domain.Entities.Identity.User, bool>> predicate, params string[] includeProperties);


        IQueryable<Domain.Entities.Identity.User> FindBy(
            Expression<Func<Domain.Entities.Identity.User, bool>> predicate,
            params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties);


        IQueryable<Domain.Entities.Identity.User> GetAll();


        IQueryable<Domain.Entities.Identity.User> GetAll(params string[] includeProperties);


        IQueryable<Domain.Entities.Identity.User> GetAll(
            params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties);




        Task<bool> IsExistAsync(Expression<Func<Domain.Entities.Identity.User, bool>> predicate);

        Task<IdentityResult> CreateAsync(Domain.Entities.Identity.User user, string password);


        Task<IdentityResult> UpdateAsync(Domain.Entities.Identity.User user);


        Task<IdentityResult> ChangePasswordAsync(Domain.Entities.Identity.User user, string oldPassword,
            string newPassword);

        Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword);

        Task<IdentityResult> DeleteAsync(Domain.Entities.Identity.User user);

        Task<IdentityResult> ConfirmEmailAsync(string userId, string token);

        Task<string> CreateEmailConfirmationTokenAsync(string userId);

        Task<string> CreatePasswordResetTokenAsync(string userId);

        Task SetLastEmailRequestDeleted(string userId);

        Task SetLastPasswordRequestDeleted(string userId);





    }
}

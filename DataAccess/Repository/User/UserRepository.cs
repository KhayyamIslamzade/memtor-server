﻿using Core.Enums;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace DataAccess.Repository.User
{
    public class UserRepository : IUserRepository, IRepositoryIdentifier
    {
        private readonly UserManager<Domain.Entities.Identity.User> _userManager;

        public UserRepository(UserManager<Domain.Entities.Identity.User> userManager)
        {
            _userManager = userManager;
        }

        public Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                return Task.FromResult<Domain.Entities.Identity.User>(null);
            return _userManager.FindByNameAsync(userName);
        }
        public Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName, params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties)
        {
            if (includeProperties == null) throw new ArgumentNullException(nameof(includeProperties));
            if (string.IsNullOrEmpty(userName))
                return Task.FromResult<Domain.Entities.Identity.User>(null);
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.UserName == userName);
        }
        public Task<Domain.Entities.Identity.User> GetUserByNameAsync(string userName, params string[] includeProperties)
        {
            if (string.IsNullOrEmpty(userName))
                return Task.FromResult<Domain.Entities.Identity.User>(null);
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.UserName == userName);
        }

        public Task<Domain.Entities.Identity.User> GetUserByEmailAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
                return Task.FromResult<Domain.Entities.Identity.User>(null);
            return _userManager.FindByEmailAsync(email);
        }

        public Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId)
        {
            return _userManager.FindByIdAsync(userId);
        }

        public Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId, params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties)
        {
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.Id == userId);
        }


        public Task<Domain.Entities.Identity.User> GetUserByIdAsync(string userId, params string[] includeProperties)
        {
            var query = _userManager.Users.IncludeAll(includeProperties);

            return query.FirstOrDefaultAsync(c => c.Id == userId);
        }

        public IQueryable<Domain.Entities.Identity.User> FindBy(Expression<Func<Domain.Entities.Identity.User, bool>> predicate)
        {
            return _userManager.Users.Where(predicate);
        }
        public IQueryable<Domain.Entities.Identity.User> FindBy(Expression<Func<Domain.Entities.Identity.User, bool>> predicate, params string[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties).Where(predicate);
        }
        public IQueryable<Domain.Entities.Identity.User> FindBy(Expression<Func<Domain.Entities.Identity.User, bool>> predicate, params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties).Where(predicate);
        }
        public IQueryable<Domain.Entities.Identity.User> GetAll()
        {
            return _userManager.Users;
        }
        public IQueryable<Domain.Entities.Identity.User> GetAll(params string[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties);
        }
        public IQueryable<Domain.Entities.Identity.User> GetAll(params Expression<Func<Domain.Entities.Identity.User, object>>[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties);
        }



        public Task<bool> IsExistAsync(Expression<Func<Domain.Entities.Identity.User, bool>> predicate)
        {
            return _userManager.Users.AnyAsync(predicate);
        }
        public Task<IdentityResult> CreateAsync(Domain.Entities.Identity.User user, string password)
        {
            return _userManager.CreateAsync(user, password);
        }
        public Task<IdentityResult> UpdateAsync(Domain.Entities.Identity.User user)
        {
            return _userManager.UpdateAsync(user);
        }
        public Task<IdentityResult> DeleteAsync(Domain.Entities.Identity.User user)
        {
            return _userManager.DeleteAsync(user);
        }

        public Task<IdentityResult> ChangePasswordAsync(Domain.Entities.Identity.User user, string oldPassword, string newPassword)
        {
            return _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        public async Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword)
        {
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);
            return await _userManager.ResetPasswordAsync(user, token, newPassword).ConfigureAwait(false);
        }
        public async Task<IdentityResult> ConfirmEmailAsync(string userId, string token)
        {
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);

            var result = await _userManager.ConfirmEmailAsync(user, token);
            return result;

        }
        public async Task<string> CreateEmailConfirmationTokenAsync(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
            var encodedToken = HttpUtility.UrlEncode(token);
            await SetLastEmailRequestDeleted(userId).ConfigureAwait(false);
            user.EmailConfirmationRequests.Add(new EmailConfirmationRequest
            {
                Token = encodedToken,
                ExpireDate = DateTime.Now.AddDays(1),

            });
            await UpdateAsync(user).ConfigureAwait(false);

            return encodedToken;
        }

        public async Task<string> CreatePasswordResetTokenAsync(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.PasswordResetRequests);

            var token = await _userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);
            var encodedToken = HttpUtility.UrlEncode(token);

            await SetLastPasswordRequestDeleted(userId).ConfigureAwait(false);

            user.PasswordResetRequests.Add(new PasswordResetRequest()
            {
                Token = encodedToken,
                ExpireDate = DateTime.Now.AddDays(1),

            });
            await UpdateAsync(user).ConfigureAwait(false);

            return encodedToken;
        }
        public async Task SetLastEmailRequestDeleted(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);
            var lastRequest = user.EmailConfirmationRequests.LastOrDefault();
            if (lastRequest != null)
                lastRequest.Status = RecordStatus.Deleted;
            await UpdateAsync(user).ConfigureAwait(false);
        }
        public async Task SetLastPasswordRequestDeleted(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.PasswordResetRequests);
            var lastRequest = user.PasswordResetRequests.LastOrDefault();
            if (lastRequest != null)
                lastRequest.Status = RecordStatus.Deleted;
            await UpdateAsync(user).ConfigureAwait(false);
        }

    }
}

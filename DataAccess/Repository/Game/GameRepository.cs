﻿using Data;

namespace DataAccess.Repository.Game
{
    public class GameRepository : Repository<Domain.Entities.Game>, IGameRepository, IRepositoryIdentifier
    {
        public GameRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

﻿namespace DataAccess.Repository.Game
{
    public interface IGameRepository : IRepository<Domain.Entities.Game>
    {
    }
}

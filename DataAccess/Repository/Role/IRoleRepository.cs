﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository.Role
{
    public interface IRoleRepository
    {

        Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId);
        Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId,
            params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties);
        Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId, params string[] includeProperties);
        IQueryable<Domain.Entities.Identity.Role> FindBy(
            Expression<Func<Domain.Entities.Identity.Role, bool>> predicate);

        IQueryable<Domain.Entities.Identity.Role> FindBy(
            Expression<Func<Domain.Entities.Identity.Role, bool>> predicate, params string[] includeProperties);
        IQueryable<Domain.Entities.Identity.Role> FindBy(
            Expression<Func<Domain.Entities.Identity.Role, bool>> predicate,
            params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties);
        IQueryable<Domain.Entities.Identity.Role> GetAll();
        IQueryable<Domain.Entities.Identity.Role> GetAll(params string[] includeProperties);
        IQueryable<Domain.Entities.Identity.Role> GetAll(
            params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties);
        Task<bool> IsExistAsync(Expression<Func<Domain.Entities.Identity.Role, bool>> predicate);
        Task<IdentityResult> CreateAsync(Domain.Entities.Identity.Role role);
        Task<IdentityResult> UpdateAsync(Domain.Entities.Identity.Role role);
        Task<IdentityResult> DeleteAsync(Domain.Entities.Identity.Role role);

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Data
{
    public static class DependencyInjection
    {

        public static IServiceCollection AddData(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureDatabase(configuration);


            return services;
        }
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(configuration.GetConnectionString("DefaultPostgre")));

            return services;
        }
    }
}

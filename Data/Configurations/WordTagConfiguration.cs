﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Configurations
{
    public class WordTagConfiguration : IEntityTypeConfiguration<WordTag>
    {
        public void Configure(EntityTypeBuilder<WordTag> modelBuilder)
        {
            modelBuilder.HasKey(table => new
            {
                table.TagId,
                table.WordId
            });

        }
    }
}

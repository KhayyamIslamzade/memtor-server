﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Configurations
{
    public class GroupWordConfiguration : IEntityTypeConfiguration<GroupWord>
    {
        public void Configure(EntityTypeBuilder<GroupWord> modelBuilder)
        {
            modelBuilder.HasKey(table => new
            {
                table.GroupId,
                table.WordId
            });

        }
    }
}

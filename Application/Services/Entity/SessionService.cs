﻿using AutoMapper;
using AutoWrapper.Wrappers;
using Core.Enums;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.Session;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Shared.Resources.Game;
using Shared.Resources.Session;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Entity
{
    public class SessionService
    {
        private readonly AuthService _authService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionRepository _repository;
        private readonly GameService _gameService;

        public SessionService(AuthService authService, IMapper mapper, IUnitOfWork unitOfWork, ISessionRepository repository, GameService gameService)
        {
            _authService = authService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = repository;
            _gameService = gameService;
        }

        public async Task<SessionGetDto> GetByIdAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id, "Group", "Owner", "Games.Word", "Games.Answers").ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            return _mapper.Map<Domain.Entities.Session, SessionGetDto>(item);
        }

        public async Task<List<SessionGetDto>> GetAllAsync()
        {
            var data = await _repository.GetAll("Group", "Owner", "Games.Word", "Games.Answers").OrderBy(c => c.DateCreated).ToListAsync();
            var result = _mapper.Map<List<Domain.Entities.Session>, List<SessionGetDto>>(data);
            return result;
        }

        public async Task<SessionGetDto> CreateAsync(SessionPostDto data)
        {

            var item = _mapper.Map<SessionPostDto, Session>(data);
            item.OwnerId = _authService.GetAuthorizedUserId();


            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await GetByIdAsync(item.Id);
        }
        public async Task<SessionGetDto> UpdateAsync(SessionPostDto data)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            if (item.SessionStatus != SessionStatusEnum.Pending)
                throw new ApiException("Session Already Started.You cant edit Session!!!");
            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return await GetByIdAsync(item.Id);
        }
        private async Task<SessionGetDto> SetSessionStatusAsync(int id, SessionStatusEnum statusEnum)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();


            item.SessionStatus = statusEnum;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return await GetByIdAsync(item.Id);
        }
        public async Task<SessionGetDto> StartSessionAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);

            if (item == null)
                throw new RecordNotFoundException();
            if (item.SessionStatus == SessionStatusEnum.Started)
                throw new ApiException("Session Already Started");

            var result = await SetSessionStatusAsync(id, SessionStatusEnum.Started);


            return result;

        }


        public async Task<GameGetDto> GetCurrentActiveGame(int sessionId)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == sessionId, "Games").ConfigureAwait(false);
            var isActiveGameExist =
                item.Games.Any(c => !c.IsCompleted);

            GameGetDto activeGame = null;

            if (isActiveGameExist)
            {
                activeGame = await _gameService.GetByPredicateAsync(c => c.SessionId == item.Id && !c.IsCompleted);
            }
            else
            {
                activeGame = await _gameService.CreateAsync(new GamePostDto(item.Id, item.GroupId));
            }
            return activeGame;

        }


        public async Task<SessionGetDto> CloseSessionAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);

            if (item == null)
                throw new RecordNotFoundException();
            if (item.SessionStatus == SessionStatusEnum.Closed)
                throw new ApiException("Session Already Closed");

            return await SetSessionStatusAsync(id, SessionStatusEnum.Closed);
        }

        public async Task DeleteAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            item.Status = RecordStatus.Deleted;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
        }


    }
}

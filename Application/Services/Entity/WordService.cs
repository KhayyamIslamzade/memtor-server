﻿using AutoMapper;
using Core.Enums;
using Core.Exceptions;
using Core.Utilities;
using DataAccess;
using DataAccess.Repository;
using DataAccess.Repository.Word;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Shared;
using Shared.Resources.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Services.Entity
{
    public class WordService
    {
        private readonly AuthService _authService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWordRepository _repository;

        public WordService(AuthService authService, IMapper mapper, IUnitOfWork unitOfWork, IWordRepository repository)
        {
            _authService = authService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public async Task<WordGetDto> GetByIdAsync(int id)
        {
            var item = (await GetAllAsync(new WordFilterParameters()
            {
                WordIds = new List<int>(id)
            }, new PagingParameters()
            {
                IsAll = true
            })).Items.FirstOrDefault();
            if (item == null)
                throw new RecordNotFoundException();
            return item;
        }

        public async Task<FilteredDataResult<WordGetDto>> GetAllAsync(WordFilterParameters filterParameters, PagingParameters pagingParameters)
        {
            var userId = _authService.GetAuthorizedUserId();
            var isAdmin = await _authService.IsAdminAsync();
            var havePermission = await _authService.UserIsInPermissionAsync(userId, $"word_{nameof(PermissionEnum.List)}");

            Expression<Func<Word, bool>> filterPredicate = null;
            Expression<Func<Word, bool>> userPredicate = null;
            Expression<Func<Word, object>> orderPredicate = null;
            if (!(isAdmin || havePermission))
                userPredicate = c => c.OwnerId == userId;
            else
                userPredicate = c => true;

            if (filterParameters != null)
            {
                var isTextExist = !string.IsNullOrEmpty(filterParameters.Text);
                var isWordIdsExist = filterParameters.WordIds != null && filterParameters.WordIds.Any();
                var isGroupIdExist = filterParameters.GroupId != null;
                filterPredicate = c =>
                    (!isTextExist || (
                        c.Title.ToLower().Contains(filterParameters.Text.ToLower()) ||
                        c.Meanings.Any(c => c.Title.ToLower().Contains(filterParameters.Text.ToLower())) ||
                        c.Tags.Any(c => c.Title.ToLower().Contains(filterParameters.Text.ToLower()))
                        )) &&
                    (!isWordIdsExist || filterParameters.WordIds.Any(id => id == c.Id));

            }


            if (filterParameters != null && filterParameters.OrderBy == "Title")
                orderPredicate = c => c.Title;
            else
                orderPredicate = c => c.DateCreated;

            var predicate = userPredicate.And(filterPredicate);
            var dataCount = await _repository.CountAsync(predicate);
            var data = _repository.FindBy(predicate, "Tags", "Meanings");
            data = data.OrderByDescending(orderPredicate).AsQueryable();

            if (!pagingParameters.IsAll)
                data = data.FindPaged(pagingParameters);



            var result = _mapper.Map<List<Domain.Entities.Word>, List<WordGetDto>>(await data.ToListAsync());

            return new FilteredDataResult<WordGetDto>()
            {
                Items = result,
                TotalCount = dataCount
            };
        }

        public async Task<WordGetDto> CreateAsync(WordPostDto data)
        {

            var item = _mapper.Map<WordPostDto, Word>(data);
            item.OwnerId = _authService.GetAuthorizedUserId();

            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await GetByIdAsync(item.Id);
        }
        public async Task<List<WordGetDto>> CreateRangeAsync(WordListPostDto data)
        {

            var userId = _authService.GetAuthorizedUserId();
            List<WordPostDto> postedWords = new List<WordPostDto>();

            if (data.SkipExistingWords)
            {
                foreach (var item in data.List)
                {
                    var isExist = await _repository.IsExistAsync(c => c.Title == item.Title && c.OwnerId == userId);
                    if (!isExist)
                    {
                        postedWords.Add(item);
                    }
                }
            }
            else
            {
                postedWords = data.List;
            }

            var list = _mapper.Map<List<WordPostDto>, List<Word>>(postedWords);
            list.ForEach(c => c.OwnerId = userId);


            await _repository.AddRangeAsync(list).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            var result = (await GetAllAsync(new WordFilterParameters()
            {
                WordIds = list.Select(c => c.Id).ToList()
            }, new PagingParameters()
            {
                IsAll = true
            })).Items;
            return result;
        }
        public async Task<WordGetDto> UpdateAsync(WordPostDto data)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id, "Tags", "Meanings").ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();

            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return await GetByIdAsync(item.Id);

        }

        public async Task DeleteAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id, "Games").ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();


            _repository.Delete(item);
            //item.Status = RecordStatus.Deleted;
            //foreach (var itemGame in item.Games.Where(c => !c.IsCompleted))
            //{
            //    itemGame.Status = RecordStatus.Deleted;

            //}
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
        }
        public async Task DeleteListAsync(List<int> ids)
        {
            var data = _repository.FindBy(c => ids.Contains(c.Id), "Games");
            foreach (var item in data)
            {
                if (item == null)
                    throw new RecordNotFoundException();
                _repository.Delete(item);
                foreach (var itemGame in item.Games.Where(c => !c.IsCompleted))
                {
                    itemGame.Status = RecordStatus.Deleted;

                }
            }
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
        }
    }
}

﻿using AutoMapper;
using Core.Enums;
using Core.Exceptions;
using Core.Utilities;
using DataAccess;
using DataAccess.Repository;
using DataAccess.Repository.Group;
using DataAccess.Repository.Word;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Resources.Group;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Services.Entity
{
    public class GroupService
    {
        private readonly AuthService _authService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGroupRepository _repository;
        private readonly IWordRepository _wordRepository;

        public GroupService(AuthService authService, IMapper mapper, IUnitOfWork unitOfWork, IGroupRepository repository, IWordRepository wordRepository)
        {
            _authService = authService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = repository;
            _wordRepository = wordRepository;
        }


        public async Task<int> GetWordCounts(int groupId)
        {
            var count = await _wordRepository.CountAsync(c => c.Groups.Any(c => c.GroupId == groupId), "Groups");
            return count;
        }

        public async Task<GroupGetDto> GetByIdAsync(int id)
        {


            var item = await _repository.GetSingleFirstAsync(c => c.Id == id, "Owner").ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            var result = _mapper.Map<Domain.Entities.Group, GroupGetDto>(item);
            result.WordCounts = await GetWordCounts(result.Id);
            return result;
        }

        public async Task<FilteredDataResult<GroupGetDto>> GetAllAsync(GroupFilterParameters filterParameters, PagingParameters pagingParameters)
        {


            var userId = _authService.GetAuthorizedUserId();
            var isAdmin = await _authService.IsAdminAsync();
            var havePermission = await _authService.UserIsInPermissionAsync(userId, $"group_{nameof(PermissionEnum.List)}");

            Expression<Func<Group, bool>> filterPredicate = null;
            Expression<Func<Group, bool>> userPredicate = null;
            Expression<Func<Group, object>> orderPredicate = null;

            if (!(isAdmin || havePermission))
                userPredicate = c => c.OwnerId == userId;
            else
                userPredicate = c => true;
            if (filterParameters != null)
            {
                var isTextExist = !string.IsNullOrEmpty(filterParameters.Text);
                filterPredicate = c =>
                    (!isTextExist || c.Title.ToLower().Contains(filterParameters.Text.ToLower()));

            }

            var predicate = userPredicate.And(filterPredicate);
            var dataCount = await _repository.CountAsync(predicate);


            var data = _repository.FindBy(predicate, "Owner").OrderByDescending(c => c.DateCreated).AsQueryable();

            if (!pagingParameters.IsAll)
                data = data.FindPaged(pagingParameters);


            var result = _mapper.Map<List<Domain.Entities.Group>, List<GroupGetDto>>(await data.ToListAsync());
            foreach (var item in result)
                item.WordCounts = await GetWordCounts(item.Id);
            return new FilteredDataResult<GroupGetDto>()
            {
                Items = result,
                TotalCount = dataCount
            };
        }

        public async Task<GroupGetDto> CreateAsync(GroupPostDto data)
        {
            var isExist = await _repository.IsExistAsync(c => c.Title == data.Title).ConfigureAwait(false);
            if (isExist)
                throw new RecordAlreadyExistException();


            var item = _mapper.Map<GroupPostDto, Group>(data);
            item.OwnerId = _authService.GetAuthorizedUserId();

            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await GetByIdAsync(item.Id);
        }
        public async Task<GroupGetDto> UpdateAsync(GroupPostDto data)
        {
            var authUserId = _authService.GetAuthorizedUserId();
            var isExist = await _repository.IsExistAsync(c => c.Title == data.Title && c.Id != data.Id && c.OwnerId != authUserId).ConfigureAwait(false);
            if (isExist)
                throw new RecordAlreadyExistException();

            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();

            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return await GetByIdAsync(item.Id);
        }

        public async Task DeleteAsync(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            item.Status = RecordStatus.Deleted;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
        }
    }
}

﻿using AutoMapper;
using Core.Enums;
using Core.Exceptions;
using Core.Utilities;
using DataAccess;
using DataAccess.Repository;
using DataAccess.Repository.Tag;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Resources.Tag;
using Shared.Resources.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Services.Entity
{
    public class TagService
    {
        private readonly AuthService _authService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITagRepository _repository;

        public TagService(AuthService authService, IMapper mapper, IUnitOfWork unitOfWork, ITagRepository repository)
        {
            _authService = authService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public async Task<TagGetDto> GetByIdAsync(int id)
        {
            var userId = _authService.GetAuthorizedUserId();
            var isAdmin = await _authService.IsAdminAsync();
            var havePermission = await _authService.UserIsInPermissionAsync(userId, $"tag_{nameof(PermissionEnum.List)}");
     

            var item = await _repository.GetSingleFirstAsync(userPredicate.And(c => c.Id == id)).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();
            return _mapper.Map<Domain.Entities.Tag, TagGetDto>(item);
        }

        public async Task<FilteredDataResult<TagGetDto>> GetAllAsync(TagFilterParameters filterParameters, PagingParameters pagingParameters)
        {
            var userId = _authService.GetAuthorizedUserId();
            var isAdmin = await _authService.IsAdminAsync();
            var havePermission = await _authService.UserIsInPermissionAsync(userId, $"tag_{nameof(PermissionEnum.List)}");
            Expression<Func<Tag, bool>> filterPredicate = null;
            Expression<Func<Tag, bool>> userPredicate = null;
            if (!(isAdmin || havePermission))
                userPredicate = c => c.OwnerId == userId;
            else
                userPredicate = c => true;

            if (filterParameters != null)
            {
                var isTextExist = !string.IsNullOrEmpty(filterParameters.Text);
                filterPredicate = c =>
                    (!isTextExist || (
                        c.Title.ToLower().Contains(filterParameters.Text.ToLower())));

            }

            var predicate = userPredicate.And(filterPredicate);
            var dataCount = await _repository.CountAsync(predicate);
            var data = _repository.FindBy(predicate).OrderByDescending(c => c.DateCreated).AsQueryable();

            if (!pagingParameters.IsAll)
                data = data.FindPaged(pagingParameters);


            var result = _mapper.Map<List<Domain.Entities.Tag>, List<TagGetDto>>(await data.ToListAsync());
            return new FilteredDataResult<TagGetDto>()
            {
                Items = result,
                TotalCount = dataCount
            };
        }

        public async Task<TagGetDto> CreateAsync(TagPostDto data)
        {

            var item = _mapper.Map<TagPostDto, Tag>(data);
            item.OwnerId = _authService.GetAuthorizedUserId();


            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await GetByIdAsync(item.Id);
        }
        public async Task<TagGetDto> UpdateAsync(TagPostDto data)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();

            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return await GetByIdAsync(item.Id);
        }

        public async Task DeleteAsync(int id)
        {
            var userId = _authService.GetAuthorizedUserId();
            var isAdmin = await _authService.IsAdminAsync();
            var havePermission = await _authService.UserIsInPermissionAsync(userId, $"tag_{nameof(PermissionEnum.Delete)}");
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);
            var isOwner = item.OwnerId == userId;
            if (item == null || !OperationDecider.CanDo(isAdmin, havePermission, isOwner))
                throw new RecordNotFoundException();

            item.Status = RecordStatus.Deleted;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
        }
    }
}

﻿using AutoMapper;
using AutoWrapper.Wrappers;
using Core.Enums;
using Core.Exceptions;
using Core.Extensions;
using DataAccess.Repository;
using DataAccess.Repository.Game;
using DataAccess.Repository.User;
using DataAccess.Repository.Word;
using Domain.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using Shared.Resources.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Services.Entity
{
    public class GameService
    {
        private readonly AuthService _authService;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGameRepository _repository;
        private readonly IWordRepository _wordRepository;

        public GameService(AuthService authService, IUserRepository userRepository, IMapper mapper, IUnitOfWork unitOfWork, IGameRepository repository, IWordRepository wordRepository)
        {
            _authService = authService;
            _userRepository = userRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = repository;
            _wordRepository = wordRepository;
        }

        public async Task<GameSettingGetDto> GetGameSettingAsync()
        {
            var userId = _authService.GetAuthorizedUserId();
            var gameSetting = (await _userRepository.GetUserByIdAsync(userId, "GameSetting")).GameSetting;

            var result = _mapper.Map<Domain.Entities.GameSetting, GameSettingGetDto>(gameSetting);
            return result;

        }
        private async Task<GameGetDto> GenerateGameByTypeAsync(Game game)
        {
            var gameSetting = await GetGameSettingAsync();
            var groupId = gameSetting.GroupId;
            var isGroupIdExist = groupId != null;



            var gameMapResult = _mapper.Map<Domain.Entities.Game, GameGetDto>(game);
            var gameType = game.GameType;
            var gameLevel = game.GameLevel;

            if (gameType == GameTypeEnum.CompletionGame)
            {
                int randomCharCount;
                int titleCount = game.Word.Title.Length;
                switch (gameLevel)
                {
                    case GameLevelEnum.Easy:
                        randomCharCount = 1;
                        break;

                    case GameLevelEnum.Medium:
                        randomCharCount = titleCount / 3;
                        break;

                    case GameLevelEnum.Hard:
                        randomCharCount = titleCount / 2;
                        break;
                    default:
                        randomCharCount = 1;
                        break;
                }
                gameMapResult.Game = new CompletionGameGetDto()
                {
                    Title = "Please fill in the blank areas",
                    Word = game.Word.Title.ReplaceRandomChars('*', randomCharCount),
                    Meanings = game.Word.Meanings.Select(c => c.Title).ToList(),
                };
            }
            //Meaning game
            else
            {
                int randomOptionLimit;
                switch (gameLevel)
                {
                    case GameLevelEnum.Easy:
                        randomOptionLimit = 3;
                        break;

                    case GameLevelEnum.Medium:
                        randomOptionLimit = 5;
                        break;

                    case GameLevelEnum.Hard:
                        randomOptionLimit = 7;
                        break;
                    default:
                        randomOptionLimit = 1;
                        break;
                }

                //Expression<Func<Word, bool>> filterPredicate = c => c.Id != game.WordId && (!isGroupIdExist ||
                //    c.Groups.Any(e => e.GroupId == groupId.Value));
                List<Word> randomWords = new List<Word>();

                Expression<Func<Word, bool>> filterPredicate = c => c.Id != game.WordId && !randomWords.Select(e => e.Id).Contains(c.Id);

                for (int i = 0; i < randomOptionLimit - 1; i++)
                {
                    var randomWord = _wordRepository.GetRandomRecords(1, filterPredicate, "Meanings").ToList().FirstOrDefault();
                    if (randomWord == null)
                    {

                    }
                    else
                    {
                        randomWords.Add(randomWord);
                    }
                }

                var options = randomWords.Select(c => c.Meanings.PickRandom().Title).ToList();
                options.Add(game.Word.Meanings.PickRandom().Title);
                options = options.Shuffle().ToList();
                gameMapResult.Game = new MeaningGameGetDto()
                {
                    Word = game.Word.Title,
                    MeaningOptions = options
                };
            }

            return gameMapResult;
        }

        public async Task<GameGetDto> GetGameAsync()
        {
            var userId = _authService.GetAuthorizedUserId();
            var game = await _repository.GetSingleFirstAsync(c => !c.IsCompleted && c.OwnerId == userId).ConfigureAwait(false);
            if (game == null)
                game = await CreateAsync();

            var result = await GetFirstAsync(c => c.Id == game.Id);

            return result;
        }

        public async Task<GameGetDto> GetFirstAsync(Expression<Func<Game, bool>> predicate)
        {
            var game = await _repository.GetSingleFirstAsync(predicate, "Owner", "Word.Meanings", "Answers").ConfigureAwait(false);
            if (game == null)
                throw new RecordNotFoundException();

            return await GenerateGameByTypeAsync(game);
        }

        public async Task<Game> CreateAsync()
        {
            var gameSetting = await GetGameSettingAsync();
            var groupId = gameSetting.GroupId;


            var game = new Game
            {
                GameLevel = gameSetting.GameLevel,
                GameType = gameSetting.GameType,
                OwnerId = _authService.GetAuthorizedUserId(),
            };

            if (gameSetting.GameLevel == GameLevelEnum.Random)
            {
                var rnd = new Random();
                var e = (GameLevelEnum)rnd.Next((int)GameLevelEnum.Easy, (int)GameLevelEnum.Hard + 1);
                game.GameLevel = e;
            }
            if (gameSetting.GameType == GameTypeEnum.Random)
            {
                var rnd = new Random();
                var e = (GameTypeEnum)rnd.Next((int)GameTypeEnum.MeaningGame, (int)GameTypeEnum.CompletionGame + 1);
                game.GameType = e;
            }



            var isGroupIdExist = groupId != null;


            var randomWord = _wordRepository.GetRandomRecords(1).ToList().FirstOrDefault();
            if (randomWord == null)
                throw new ApiException("add at least 1 word required!");
            game.Word = randomWord;
            await _repository.AddAsync(game).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return game;
        }

        public async Task<GameGetDto> PassGameAsync(int gameId)
        {
            var game = await _repository.GetSingleFirstAsync(c => c.Id == gameId && !c.IsCompleted, "Owner", "Word.Meanings", "Answers").ConfigureAwait(false);

            if (game == null)
                throw new RecordNotFoundException();

            game.AnswerStatus = AnswerStatusEnum.Passed;
            game.IsCompleted = true;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            var result = await GetFirstAsync(c => c.Id == game.Id);

            return result;
        }
        public async Task<GameAnswerGetDto> CheckAnswerAsync(GameAnswerPostDto data)
        {
            var userId = _authService.GetAuthorizedUserId();
            var game = await _repository.GetSingleFirstAsync(c => c.Id == data.Id && !c.IsCompleted, "Owner", "Word.Meanings", "Answers").ConfigureAwait(false);

            if (game == null)
                throw new RecordNotFoundException();


            var isTrue = false;
            if (game.Word != null)
            {
                if (game.GameType == GameTypeEnum.CompletionGame)
                {
                    isTrue = game.Word.Title.Equals(data.Answer, StringComparison.InvariantCultureIgnoreCase);

                }
                else if (game.GameType == GameTypeEnum.MeaningGame)
                {
                    if (game.Word.Meanings.Any())
                        isTrue = game.Word.Meanings.Any(c =>
                            c.Title.Equals(data.Answer, StringComparison.InvariantCultureIgnoreCase));
                }
            }


            game.IsCompleted = true;
            game.AnswerStatus = AnswerStatusEnum.Answered;
            game.IsTrueAnswer = isTrue;
            game.Answers.Add(new Answer()
            {
                Label = data.Answer,
                OwnerId = userId,
                IsTrue = isTrue

            });
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);
            return new GameAnswerGetDto()
            {
                Answer = data.Answer,
                IsTrue = isTrue
            };
        }

        public async Task<GameSettingGetDto> UpdateGameSettingAsync(GameSettingPostDto data)
        {
            var userId = _authService.GetAuthorizedUserId();
            var user = (await _userRepository.GetUserByIdAsync(userId, "GameSetting"));
            _mapper.Map(data, user.GameSetting);
            await _userRepository.UpdateAsync(user);
            return await GetGameSettingAsync();
        }

    }
}

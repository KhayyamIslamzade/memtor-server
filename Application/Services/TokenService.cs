﻿using AutoMapper;
using AutoWrapper.Wrappers;
using Core.Constants;
using Core.Utilities;
using DataAccess.Repository.User;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class TokenService
    {
        private readonly IConfiguration _configuration;
        private readonly SignInManager<User> _signInManager;
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly TokenSettings _tokenSettings;

        public TokenService(IConfiguration configuration, IOptions<TokenSettings> tokenSettings, SignInManager<User> signInManager, IUserRepository repository, IMapper mapper)
        {
            _configuration = configuration;
            _signInManager = signInManager;
            _repository = repository;
            _mapper = mapper;
            _tokenSettings = tokenSettings.Value;

        }

        public async Task<string> GetTokenAsync(string emailOrPassword, bool isUsername, string password)
        {

            var userQuery = _repository.GetAll(new IncludeStringConstants().UserRolePermissionIncludeArray);

            User user;
            switch (isUsername)
            {
                case true:
                    user = await userQuery.FirstOrDefaultAsync(c => c.UserName == emailOrPassword)
                        .ConfigureAwait(false);
                    break;
                case false:
                    user = await userQuery.FirstOrDefaultAsync(c => c.Email == emailOrPassword)
                        .ConfigureAwait(false);
                    break;
            }

            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, password, false).ConfigureAwait(false);

                if (result.Succeeded)
                {

                    return GenerateToken(user);
                }
            }
            throw new ApiException(MessageBuilder.LoginFault, StatusCodes.Status400BadRequest);
        }
        public string GenerateToken(User user)
        {

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString().Substring(0,16)),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
            };

            var appSettings = _tokenSettings;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JwtKey));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expires = DateTime.Now.AddDays(Convert.ToDouble(appSettings.JwtExpireDays));

            var token = new JwtSecurityToken(appSettings.JwtIssuer, appSettings.JwtIssuer, claims, expires: expires,
                signingCredentials: credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
    }
}

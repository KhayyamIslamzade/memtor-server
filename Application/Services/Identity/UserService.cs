﻿using AutoMapper;
using AutoWrapper.Wrappers;
using Core.Constants;
using Core.Exceptions;
using Core.Utilities;
using DataAccess.Repository.User;
using Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Resources.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Identity
{
    public class UserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }


        #region CRUD

        public async Task<User> GetAsync(string id)
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var item = await _userRepository.GetUserByIdAsync(id, includeParams.ToArray()).ConfigureAwait(false);
            if (item == null)
                throw new RecordNotFoundException();

            return item;
        }

        public async Task<UserGetDto> GetMappedAsync(string id)
        {
            var result = _mapper.Map<User, UserGetDto>(await GetAsync(id));
            return result;
        }

        public async Task<List<User>> GetAllAsync()
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var data = await _userRepository.FindBy(c => c.IsEditable, includeParams.ToArray()).ToListAsync();
            return data;
        }

        public async Task<List<UserGetDto>> GetAllMappedAsync()
        {
            return _mapper.Map<List<User>, List<UserGetDto>>(await GetAllAsync());
        }
        public async Task<User> CreateAsync(UserAddPostDto data)
        {
            var isExistAsync = await _userRepository.IsExistAsync(c => c.UserName == data.UserName || c.Email == data.Email)
                .ConfigureAwait(false);
            if (isExistAsync)
                throw new RecordAlreadyExistException();
            var user = _mapper.Map<UserAddPostDto, User>(data);

            user.Id = Guid.NewGuid().ToString();
            user.EmailConfirmed = true;

            var result = await _userRepository.CreateAsync(user, data.Password).ConfigureAwait(false);
            if (result.Succeeded)
                return user;
            var errorMessage = result.Errors.FirstOrDefault();
            throw new ApiException(errorMessage);
        }

        public async Task<User> UpdateAsync(UserEditPostDto data)
        {

            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var user = await _userRepository.GetUserByIdAsync(data.Id, includeParams.ToArray()).ConfigureAwait(false);

            if (user == null)
                throw new RecordNotFoundException();
            if (!user.IsEditable)
                throw new RecordNotEditableException();
            //update
            _mapper.Map<UserEditPostDto, User>(data, user);

            await _userRepository.UpdateAsync(user).ConfigureAwait(false);
            return user;
        }

        public async Task<bool> IsUserNameExist(string username)
        {
            return await _userRepository.IsExistAsync(c => c.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase)).ConfigureAwait(false);
        }
        public async Task<bool> IsEmailExist(string email)
        {
            return await _userRepository.IsExistAsync(c => c.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)).ConfigureAwait(false);
        }
        public async Task ChangePasswordAsync(UserChangePasswordPostDto data)
        {
            var user = await _userRepository.GetUserByIdAsync(data.Id).ConfigureAwait(false);
            if (user == null)
                throw new RecordNotFoundException();
            if (!user.IsEditable)
                throw new RecordNotEditableException();


            var changePasswordResult =
                await _userRepository.ChangePasswordAsync(user, data.OldPassword, data.Password).ConfigureAwait(false);
            if (!changePasswordResult.Succeeded)
            {
                var errorMessage = changePasswordResult.Errors.FirstOrDefault();
                var message = errorMessage == null ? MessageBuilder.Fail : errorMessage.Description;
                throw new ApiException(message);
            }
        }

        public async Task DeleteAsync(string id)
        {
            var user = await _userRepository.GetUserByIdAsync(id).ConfigureAwait(false);
            if (user == null)
                throw new RecordNotFoundException();
            if (!user.IsEditable)
                throw new RecordNotEditableException();

            await _userRepository.DeleteAsync(user).ConfigureAwait(false);
        }

        #endregion




    }
}

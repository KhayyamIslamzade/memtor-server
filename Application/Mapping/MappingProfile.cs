﻿using AutoMapper;
using Core.Extensions;
using Domain.Entities;
using Domain.Entities.Identity;
using Shared.Resources.Answer;
using Shared.Resources.Game;
using Shared.Resources.Permission;
using Shared.Resources.PermissionCategory;
using Shared.Resources.Role;
using Shared.Resources.Tag;
using Shared.Resources.User;
using Shared.Resources.Word;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region User
            CreateMap<User, UserGetDto>()
                .ForMember(er => er.Roles,
                    opt => opt.MapFrom(e => e.Roles.Select(c => c.Role)))
                .ForMember(er => er.DirectivePermissions,
                    opt => opt.MapFrom(e => e.DirectivePermissions.Select(c => c.Permission)))
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)))
                ;
            CreateMap<User, UserInfoGetDto>();
            CreateMap<UserRegisterPostDto, User>();



            CreateMap<UserAddPostDto, User>()
                .ForMember(c => c.Roles, opt => opt.Ignore())
                .ForMember(c => c.DirectivePermissions, opt => opt.Ignore())

                .AfterMap((userAdd, user) =>
                {

                    var addedRoles = userAdd.Roles.Where(id => user.Roles.All(f => f.RoleId != id)).Select(c => new UserRole() { RoleId = c, DateCreated = DateTime.Now });
                    foreach (var item in addedRoles)
                        user.Roles.Add(item);

                    var addedPermissions = userAdd.DirectivePermissions.Where(id => user.DirectivePermissions.All(f => f.PermissionId != id)).Select(c => new UserPermission() { PermissionId = c, DateCreated = DateTime.Now });
                    foreach (var item in addedPermissions)
                        user.DirectivePermissions.Add(item);
                });
            CreateMap<UserEditPostDto, User>()
                .ForMember(c => c.Roles, opt => opt.Ignore())
                .ForMember(c => c.DirectivePermissions, opt => opt.Ignore())

                .AfterMap((userAdd, user) =>
                {
                    var removedRoles = user.Roles.Where(f => !userAdd.Roles.Contains(f.RoleId)).ToList();
                    foreach (var item in removedRoles)
                    {
                        user.Roles.Remove(item);
                    }

                    if (userAdd.Roles != null && userAdd.Roles.Any())
                    {
                        var addedRoles = userAdd.Roles.Where(id => user.Roles.All(f => f.RoleId != id)).Select(c => new UserRole() { RoleId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedRoles)
                        {
                            user.Roles.Add(item);
                        }
                    }


                    var removedPermissions = user.DirectivePermissions.Where(f => !userAdd.DirectivePermissions.Contains(f.PermissionId)).ToList();
                    foreach (var item in removedPermissions)
                    {
                        user.DirectivePermissions.Remove(item);
                    }

                    if (userAdd.DirectivePermissions != null && userAdd.DirectivePermissions.Any())
                    {
                        var addedPermissions = userAdd.DirectivePermissions.Where(id => user.DirectivePermissions.All(f => f.PermissionId != id)).Select(c => new UserPermission() { PermissionId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedPermissions)
                        {
                            user.DirectivePermissions.Add(item);
                        }
                    }
                });
            #endregion

            #region Role
            CreateMap<Role, RoleGetDto>()
                .ForMember(c => c.Permissions, opt => opt.MapFrom(m => m.PermissionCategory.Select(c => c.PermissionCategoryPermission)))
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<RolePostDto, Role>()
                .ForMember(c => c.PermissionCategory, opt => opt.Ignore())
                .AfterMap((roleAdd, role) =>
                {
                    var removedPermissions = role.PermissionCategory.Where(f => !roleAdd.PermissionCategories.Contains(f.PermissionCategoryPermissionId)).ToList();
                    foreach (var item in removedPermissions)
                    {
                        role.PermissionCategory.Remove(item);
                    }

                    if (roleAdd.PermissionCategories != null && roleAdd.PermissionCategories.Any())
                    {
                        var addedPermissions = roleAdd.PermissionCategories.Where(id => role.PermissionCategory.All(f => f.PermissionCategoryPermissionId != id)).Select(c => new RolePermissionCategory() { PermissionCategoryPermissionId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedPermissions)
                        {
                            role.PermissionCategory.Add(item);
                        }
                    }


                });

            #endregion

            #region Permission

            CreateMap<Permission, PermissionGetDto>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            #endregion   
            #region Permission Category

            CreateMap<PermissionCategory, PermissionCategoryGetDto>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<Permission, PermissionGetDto>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));


            CreateMap<PermissionCategoryPermission, PermissionCategoryRelationGetDto>()
                .ForMember(c => c.RelationId, opt => opt.MapFrom(m => m.Id))
                .ForMember(c => c.Category, opt => opt.MapFrom(m => m.Category))
                .ForMember(c => c.Permission, opt => opt.MapFrom(m => m.Permission));
            #endregion



            #region Word
            CreateMap<Word, WordGetDto>()
                .ForMember(c => c.Meanings, opt => opt.MapFrom(m => m.Meanings != null && m.Meanings.Any() ? m.Meanings.Select(e => e.Title) : new List<string>()))
                .ForMember(c => c.Tags, opt => opt.MapFrom(m => m.Tags != null && m.Tags.Any() ? m.Tags.Select(e => e.Title) : new List<string>()))

                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<WordPostDto, Word>()
                .ForMember(c => c.Title, opt => opt.MapFrom(m => m.Title.TrimStart().TrimEnd()))
                .ForMember(c => c.Meanings, opt => opt.Ignore())
                .ForMember(c => c.Tags, opt => opt.Ignore())
                .AfterMap((dto, entity) =>
                {
                    var removedMeanings = entity.Meanings.Where(f => !dto.Meanings.Contains(f.Title)).ToList();
                    foreach (var item in removedMeanings)
                    {
                        entity.Meanings.Remove(item);
                    }

                    if (dto.Meanings != null && dto.Meanings.Any())
                    {
                        var addedMeanings = dto.Meanings.Where(title => entity.Meanings.All(f => f.Title != title)).Select(c => new Meaning() { Title = c }).ToList();
                        foreach (var item in addedMeanings)
                        {
                            entity.Meanings.Add(item);
                        }
                    }


                    var removedTags = entity.Tags.Where(f => !dto.Tags.Contains(f.Title)).ToList();
                    foreach (var item in removedTags)
                    {
                        entity.Tags.Remove(item);
                    }

                    if (dto.Tags != null && dto.Tags.Any())
                    {
                        var addedTags = dto.Tags.Where(title => entity.Tags.All(f => f.Title != title)).Select(c => new Tag()
                        {
                            Title = c
                        }).ToList();
                        foreach (var item in addedTags)
                        {
                            entity.Tags.Add(item);
                        }
                    }
                }); ;
            #endregion

            #region Tag


            CreateMap<Tag, TagGetDto>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));

            CreateMap<TagPostDto, Tag>();
            #endregion

            #region GameSetting
            CreateMap<GameSetting, GameSettingGetDto>();
            CreateMap<GameSettingPostDto, GameSetting>();
            #endregion

            #region Game
            CreateMap<Game, GameGetDto>()
                .ForMember(c => c.Game, opt => opt.Ignore())
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<GamePostDto, Game>();
            #endregion

            #region Answer
            CreateMap<Answer, AnswerGetDto>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<AnswerPostDto, Answer>();
            #endregion
        }
    }
}

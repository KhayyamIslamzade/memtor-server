﻿using System.Collections.Generic;

namespace Shared.Resources.Game
{
    public class MeaningGameGetDto : IGameType
    {
        public string Title
        {
            get
            {
                return $"Find out which of the following means {Word}";
            }
        }

        public string Word { get; set; }
        public List<string> MeaningOptions { get; set; }
    }
}

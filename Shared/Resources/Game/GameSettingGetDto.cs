﻿using Core.Enums;
using Shared.Resources.Group;

namespace Shared.Resources.Game
{
    public class GameSettingGetDto
    {
        public GameLevelEnum GameLevel { get; set; }
        public GameTypeEnum GameType { get; set; }
        public int? GroupId { get; set; }
        public GroupGetDto Group { get; set; }
    }
}

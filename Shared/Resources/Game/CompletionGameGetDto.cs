﻿using System.Collections.Generic;

namespace Shared.Resources.Game
{
    public class CompletionGameGetDto : IGameType
    {
        public string Title { get; set; }
        public string Word { get; set; }
        public List<string> Meanings { get; set; }
    }
}

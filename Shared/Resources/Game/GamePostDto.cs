﻿namespace Shared.Resources.Game
{
    public class GamePostDto
    {
        public GamePostDto(int sessionId, int? groupId)
        {
            SessionId = sessionId;
            GroupId = groupId;
        }

        public int SessionId { get; set; }
        public int? GroupId { get; set; }
    }
}

﻿namespace Shared.Resources.Game
{
    public class GameAnswerPostDto
    {
        public int Id { get; set; }
        public string Answer { get; set; }
    }
}

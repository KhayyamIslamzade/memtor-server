﻿using Core.Enums;
using Shared.Resources.Answer;
using System.Collections.Generic;

namespace Shared.Resources.Game
{
    public interface IGameGetDto
    {
        public int Id { get; set; }
        public AnswerStatusEnum AnswerStatus { get; set; }
        public int RetryCount { get; set; }
        public List<AnswerGetDto> Answers { get; set; }
        public string DateCreated { get; set; }
    }
}

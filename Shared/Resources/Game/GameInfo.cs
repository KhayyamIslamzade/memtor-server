﻿namespace Shared.Resources.Game
{
    public class GameInfo
    {
        public int TotalWordCount { get; set; }
        public bool IsPlayable
        {
            get
            {
                return TotalWordCount > 0;
            }
            private set { }

        }
    }
}

﻿using Core.Enums;

namespace Shared.Resources.Game
{
    public class GameSettingPostDto
    {
        public GameLevelEnum GameLevel { get; set; } = GameLevelEnum.Easy;
        public GameTypeEnum GameType { get; set; } = GameTypeEnum.MeaningGame;
        public int? GroupId { get; set; }
    }
}

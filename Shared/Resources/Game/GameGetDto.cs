﻿using Core.Enums;
using Shared.Resources.Answer;
using System.Collections.Generic;

namespace Shared.Resources.Game
{
    public class GameGetDto : IGameGetDto
    {
        public int Id { get; set; }
        public AnswerStatusEnum AnswerStatus { get; set; }
        public GameLevelEnum GameLevel { get; set; }
        public GameTypeEnum GameType { get; set; }
        public int RetryCount { get; set; }
        public List<AnswerGetDto> Answers { get; set; } = new List<AnswerGetDto>();
        public string DateCreated { get; set; }

        public object Game { get; set; }

    }
}

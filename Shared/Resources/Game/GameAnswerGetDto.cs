﻿namespace Shared.Resources.Game
{
    public class GameAnswerGetDto
    {
        public bool IsTrue { get; set; }
        public string Answer { get; set; }
    }
}

﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Tag
{
    public class TagPostDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(StringLengthConstants.LengthXxxs)]
        public string Title { get; set; }
    }
}

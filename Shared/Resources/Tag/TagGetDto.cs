﻿using Shared.Resources.User;

namespace Shared.Resources.Tag
{
    public class TagGetDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public UserInfoGetDto Owner { get; set; }
        public string DateCreated { get; set; }
        public bool IsConst { get; set; }
    }
}

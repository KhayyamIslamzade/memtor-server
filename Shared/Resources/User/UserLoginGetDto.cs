﻿namespace Shared.Resources.User
{
    public class UserLoginGetDto
    {
        public string Token { get; set; }
    }
}

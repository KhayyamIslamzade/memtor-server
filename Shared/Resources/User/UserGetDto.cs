﻿using Shared.Resources.Permission;
using Shared.Resources.Role;
using System.Collections.Generic;

namespace Shared.Resources.User
{
    public class UserGetDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool IsEditable { get; set; }
        public string DateCreated { get; set; }
        public List<RoleGetDto> Roles { get; set; } = new List<RoleGetDto>();
        public List<PermissionGetDto> DirectivePermissions { get; set; } = new List<PermissionGetDto>();

    }
}

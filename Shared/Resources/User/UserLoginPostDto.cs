﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.User
{
    public class UserLoginPostDto
    {

        [Required(ErrorMessage = "Email or Username is not defined")]
        public string EmailOrUsername { get; set; }

        [Required(ErrorMessage = "Password is not defined")]
        public string Password { get; set; }

    }
}

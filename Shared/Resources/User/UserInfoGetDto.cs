﻿namespace Shared.Resources.User
{
    public class UserInfoGetDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

    }
}

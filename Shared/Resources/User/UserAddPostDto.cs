﻿using System.Collections.Generic;

namespace Shared.Resources.User
{
    public class UserAddPostDto : UserRegisterPostDto
    {
        public List<string> Roles { get; set; }
        public List<string> DirectivePermissions { get; set; }

        public UserAddPostDto()
        {
            Roles = new List<string>();
            DirectivePermissions = new List<string>();
        }

    }
}

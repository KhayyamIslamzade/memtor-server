﻿namespace Shared.Resources.Answer
{
    public class AnswerPostDto
    {
        public int GameId { get; set; }
        public string Label { get; set; }
    }
}

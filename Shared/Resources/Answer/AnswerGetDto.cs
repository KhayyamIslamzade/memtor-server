﻿namespace Shared.Resources.Answer
{
    public class AnswerGetDto
    {
        public int Id { get; set; }
        //answer
        public string Label { get; set; }
        public bool IsTrue { get; set; }
        public string DateCreated { get; set; }
    }
}

﻿using Shared.Resources.User;

namespace Shared.Resources.Group
{
    public class GroupGetDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsPublic { get; set; }
        public UserInfoGetDto Owner { get; set; }
        public string DateCreated { get; set; }
        public int WordCounts { get; set; }
    }
}

﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Group
{
    public class GroupPostDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(StringLengthConstants.LengthMd)]
        public string Title { get; set; }
        public bool IsPublic { get; set; }
    }
}

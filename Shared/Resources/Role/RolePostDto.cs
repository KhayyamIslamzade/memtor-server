﻿using Core.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Role
{
    public class RolePostDto
    {

        public string Id { get; set; }
        [Required]
        [StringLength(StringLengthConstants.LengthSm)]
        public string Name { get; set; }
        public List<int> PermissionCategories { get; set; } = new List<int>();

    }
}

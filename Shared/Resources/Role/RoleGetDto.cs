﻿using Shared.Resources.PermissionCategory;
using System.Collections.Generic;

namespace Shared.Resources.Role
{
    public class RoleGetDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string Description { get; set; }
        public List<PermissionCategoryRelationGetDto> Permissions { get; set; }
        public bool IsEditable { get; set; }
    }
}

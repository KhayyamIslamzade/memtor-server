﻿using Core.Enums;
using Shared.Resources.Game;
using Shared.Resources.Group;
using Shared.Resources.User;
using System.Collections.Generic;

namespace Shared.Resources.Session
{
    public class SessionGetDto
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public GameLevelEnum GameLevel { get; set; }
        public GameTypeEnum GameType { get; set; }

        public SessionStatusEnum SessionStatus { get; set; }
        public GroupGetDto Group { get; set; }
        public UserInfoGetDto Owner { get; set; }
        public List<GameGetDto> Games { get; set; }
        public string DateCreated { get; set; }
    }
}

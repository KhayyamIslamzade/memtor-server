﻿using Core.Enums;

namespace Shared.Resources.Session
{
    public class SessionPostDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public GameLevelEnum GameLevel { get; set; } = GameLevelEnum.Easy;
        public GameTypeEnum GameType { get; set; } = GameTypeEnum.MeaningGame;
        public int? GroupId { get; set; }
    }
}

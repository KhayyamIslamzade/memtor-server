﻿namespace Shared.Resources.PermissionCategory
{
    public class PermissionCategoryGetDto
    {

        public string Label { get; set; }
        public string VisibleLabel { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
    }
}

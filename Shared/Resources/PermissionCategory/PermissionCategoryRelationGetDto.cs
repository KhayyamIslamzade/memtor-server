﻿using Shared.Resources.Permission;

namespace Shared.Resources.PermissionCategory
{
    public class PermissionCategoryRelationGetDto
    {
        public int RelationId { get; set; }
        public PermissionCategoryGetDto Category { get; set; }
        public PermissionGetDto Permission { get; set; }

    }
}

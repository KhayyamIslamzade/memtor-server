﻿namespace Shared.Resources.Permission
{
    public class PermissionGetDto
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
    }
}

﻿using Core.Attributes;
using Core.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Word
{
    public class WordPostDto
    {
        public int? GroupId { get; set; }
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        [StringLength(StringLengthConstants.LengthXs)]
        public string Title { get; set; }
        [StringLength(StringLengthConstants.LengthLg)]
        public string Description { get; set; }
        [MinCountOfElements(1, ErrorMessage = "At Least 1 Meaning is required!")]
        public List<string> Meanings { get; set; }
        public List<string> Tags { get; set; }
    }
}

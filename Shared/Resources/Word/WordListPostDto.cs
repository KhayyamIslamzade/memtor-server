﻿using System.Collections.Generic;

namespace Shared.Resources.Word
{
    public class WordListPostDto
    {
        public List<WordPostDto> List { get; set; }
        public bool SkipExistingWords { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Shared.Resources.Word
{
    public class WordGetDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Meanings { get; set; }
        public List<string> Tags { get; set; }
        public string DateCreated { get; set; }

    }
}

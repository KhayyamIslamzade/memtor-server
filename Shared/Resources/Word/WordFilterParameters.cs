﻿using Core.Attributes;
using System.Collections.Generic;

namespace Shared.Resources.Word
{
    public class WordFilterParameters
    {
        public int? GroupId { get; set; }
        public string Text { get; set; }
        public List<int>? WordIds { get; set; }
        [StringValueLimit("Date", "Title", ErrorMessage = "Values must be Date , Title")]
        public string OrderBy { get; set; }
    }
}

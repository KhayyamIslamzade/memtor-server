﻿using Application.Services.Entity;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.Session;
using System.Threading.Tasks;

namespace Api.Controllers.Entity
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class SessionController : ControllerBase
    {
        private readonly SessionService _service;

        public SessionController(SessionService service)
        {
            _service = service;
        }
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetByIdAsync(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return new ApiResponse(result);
        }

        [HttpGet]
        public async Task<ApiResponse> GetAllAsync()
        {
            var data = await _service.GetAllAsync();

            return new ApiResponse(data);

        }
        [HttpGet("{id}/GetActiveGame")]
        public async Task<ApiResponse> GetActiveGameAsync(int id)
        {
            var result = await _service.GetCurrentActiveGame(id);
            return new ApiResponse(result);
        }

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] SessionPostDto data)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _service.CreateAsync(data);

            return new ApiResponse(result);
        }
        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] SessionPostDto data)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _service.UpdateAsync(data);

            return new ApiResponse(result);
        }
        [HttpPut("{id}/Start")]
        public async Task<ApiResponse> StartSessionAsync(int id)
        {
            var result = await _service.StartSessionAsync(id);
            return new ApiResponse(result);
        }
        [HttpPut("{id}/Close")]
        public async Task<ApiResponse> CloseSessionAsync(int id)
        {
            var result = await _service.CloseSessionAsync(id);
            return new ApiResponse(result);
        }


        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(int id)
        {

            await _service.DeleteAsync(id);

            return new ApiResponse();
        }

    }
}

﻿using Application.Services.Entity;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.Game;
using System.Threading.Tasks;

namespace Api.Controllers.Entity
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class GameController : ControllerBase
    {
        private readonly GameService _service;

        public GameController(GameService service)
        {
            _service = service;
        }


        [HttpGet]
        public async Task<ApiResponse> GetAsync()
        {
            var data = await _service.GetGameAsync();

            return new ApiResponse(data);

        }
        [HttpGet("GetGameSetting")]
        public async Task<ApiResponse> GetGameSettingAsync()
        {
            var data = await _service.GetGameSettingAsync();

            return new ApiResponse(data);

        }
        [HttpPut("GameSetting")]
        public async Task<ApiResponse> UpdateGameSettingAsync([FromBody] GameSettingPostDto data)
        {
            var result = await _service.UpdateGameSettingAsync(data);
            return new ApiResponse(result);

        }
        [HttpPost("CheckAnswer")]
        public async Task<ApiResponse> CheckAnswerAsync([FromBody] GameAnswerPostDto data)
        {
            var result = await _service.CheckAnswerAsync(data);
            return new ApiResponse(result);

        }
        [HttpPut("{gameId}/Pass")]
        public async Task<ApiResponse> CheckAnswerAsync(int gameId)
        {
            var result = await _service.PassGameAsync(gameId);
            return new ApiResponse(result);

        }

    }
}

﻿using Application.Services.Entity;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Shared.Resources.Group;
using System.Threading.Tasks;

namespace Api.Controllers.Entity
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class GroupController : ControllerBase
    {
        private readonly GroupService _service;

        public GroupController(GroupService service)
        {
            _service = service;
        }
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetByIdAsync(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return new ApiResponse(result);
        }

        [HttpGet]
        public async Task<ApiResponse> GetAllAsync([FromQuery] GroupFilterParameters filterParameters, [FromQuery] PagingParameters pagingParameters)
        {
            var data = await _service.GetAllAsync(filterParameters, pagingParameters);

            return new ApiResponse(data);

        }

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] GroupPostDto data)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _service.CreateAsync(data);

            return new ApiResponse(result);
        }
        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] GroupPostDto data)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _service.UpdateAsync(data);

            return new ApiResponse(result);
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(int id)
        {

            await _service.DeleteAsync(id);

            return new ApiResponse();
        }

    }
}

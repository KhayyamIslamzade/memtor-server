﻿using Application.Middlewares;
using Application.Services.Entity;
using AutoWrapper.Wrappers;
using Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TestController : ControllerBase
    {
        private readonly GameService _gameService;

        public TestController(GameService gameService)
        {
            _gameService = gameService;
        }
        [HttpGet("AdminOnly")]
        [Permission(true, "admin", "moderator")]
        public ApiResponse AdminOnly()
        {
            return new ApiResponse(MessageBuilder.Successfully, StatusCodes.Status200OK);
        }
        [AllowAnonymous]
        [HttpGet("GetGame/{id}")]
        public async Task<ApiResponse> GetGame(int id)
        {
            var result = await _gameService.GetFirstAsync(c => c.Id == id);
            return new ApiResponse(result);
        }
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse Get(int id)
        {
            return new ApiResponse(new string[]
            {
                "value 1",
                "value 2",
                "value 3"
            });
        }

    }
}

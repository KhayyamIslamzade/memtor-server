﻿using Application.Services.Identity;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.Controllers.Identity
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PermissionController : ControllerBase
    {
        private readonly PermissionService _service;

        public PermissionController(PermissionService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<ApiResponse> GetAllAsync()
        {
            var result = await _service.GetAllAsync();
            return new ApiResponse(result);
        }


        [HttpGet("GetAllDirectivePermissions")]
        public async Task<ApiResponse> GetAllDirectivePermissionsAsync()
        {
            var result = await _service.GetAllDirectivePermissionsAsync();
            return new ApiResponse(result);
        }

    }
}

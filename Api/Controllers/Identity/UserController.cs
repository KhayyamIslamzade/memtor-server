﻿using Application.Services.Identity;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Core.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.User;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Api.Controllers.Identity
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetAsync(string id)
        {
            var result = await _userService.GetAsync(id);
            return new ApiResponse(result);

        }
        [HttpGet]
        public async Task<ApiResponse> GetAllAsync()
        {
            var result = await _userService.GetAllMappedAsync();
            return new ApiResponse(result);

        }

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] UserAddPostDto data)
        {

            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _userService.CreateAsync(data);
            return new ApiResponse(await GetAsync(result.Id));

        }
        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] UserEditPostDto data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _userService.UpdateAsync(data);
            return new ApiResponse(await GetAsync(result.Id));
        }


        [HttpPost("ChangePassword")]
        public async Task<ApiResponse> ChangePasswordAsync([FromBody] UserChangePasswordPostDto data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            await _userService.ChangePasswordAsync(data);
            return new ApiResponse(result: null);

        }
        [AllowAnonymous]
        [HttpGet("IsUsernameExist")]
        public async Task<ApiResponse> IsUsernameExistAsync([Required][FromQuery] string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new RecordNotFoundException();
            return new ApiResponse(await _userService.IsUserNameExist(username));

        }
        [AllowAnonymous]
        [HttpGet("IsEmailExist")]
        public async Task<ApiResponse> IsEmailExistAsync([Required][FromQuery] string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new RecordNotFoundException();
            return new ApiResponse(await _userService.IsEmailExist(email));
        }


        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(string id)
        {
            await _userService.DeleteAsync(id);
            return new ApiResponse(result: null);
        }

    }
}

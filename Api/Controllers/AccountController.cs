﻿using Application.Services;
using Application.Services.Identity;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Core.Extensions;
using Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.User;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly AuthService _authService;
        private readonly TokenService _tokenService;

        public AccountController(AuthService authService, UserService userService,
            TokenService tokenService)
        {
            _userService = userService;
            _authService = authService;
            _tokenService = tokenService;
        }
        [HttpPost("GetToken")]
        public async Task<ApiResponse> GetTokenAsync([FromBody] UserLoginPostDto data)
        {

            var isEmail = data.EmailOrUsername.IsEmail();
            var isUsername = data.EmailOrUsername.IsUsername();

            if (!(isEmail || isUsername))
                ModelState.AddModelError("emailOrUsername", "Enter valid Email/Username");

            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _tokenService.GetTokenAsync(data.EmailOrUsername, isUsername, data.Password);

            return new ApiResponse(new
            {
                Token = result
            });
        }
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("GetAuthorizedUser")]
        public async Task<ApiResponse> GetAuthorizedAsync()
        {
            var result = await _authService.GetAuthUserMappedAsync();
            return new ApiResponse(result);

        }
        [HttpPost("Register")]
        public async Task<ApiResponse> Register([FromBody] UserRegisterPostDto data)
        {

            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            await _userService.CreateAsync(new UserAddPostDto()
            {
                UserName = data.UserName,
                Email = data.Email,
                Password = data.Password,
                ConfirmPassword = data.ConfirmPassword
            });

            return new ApiResponse(await GetTokenAsync(new UserLoginPostDto()
            {
                EmailOrUsername = data.Email,
                Password = data.Password
            }));
        }

        [HttpGet("ConfirmEmail")]
        public async Task<ApiResponse> ConfirmEmail([FromQuery][Required] string userId, [FromQuery][Required] string token)
        {
            await _authService.ConfirmEmailAsync(userId, token).ConfigureAwait(false);
            return new ApiResponse(MessageBuilder.EmailConfirmed, StatusCodes.Status200OK);
        }

        [HttpGet("ForgotPassword")]
        public async Task<ApiResponse> ForgotPassword([Required] string email)
        {
            await _authService.ForgotPasswordAsync(email);
            return new ApiResponse();
        }
        [HttpGet("CheckPasswordReset")]
        public async Task<ApiResponse> PasswordReset([FromQuery][Required] string userId)
        {
            await _authService.CheckPasswordResetRequestAsync(userId);
            return new ApiResponse(MessageBuilder.Success, StatusCodes.Status200OK);
        }
        [HttpPost("PasswordReset")]
        public async Task<ApiResponse> PasswordReset(PasswordResetPostDto data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            await _authService.PasswordResetAsync(data);
            return new ApiResponse(MessageBuilder.PasswordReset, StatusCodes.Status200OK);
        }

    }
}

﻿using Application;
using Application.Middlewares;
using AutoMapper;
using AutoWrapper;
using Core;
using Data;
using DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Shared;
using System.Reflection;
using System.Text.Json;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;
        }

        public IConfiguration Configuration { get; }
        public static IConfiguration StaticConfig { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            }); ;

            services.AddAutoMapper(
                typeof(Application.DependencyInjection).GetTypeInfo().Assembly,
                GetType().Assembly
            );

            services.Configure<EmailSettings>(Configuration.GetSection("emailSettings"));
            services.Configure<TokenSettings>(Configuration.GetSection("tokenSettings"));
            services.Configure<DomainSettings>(Configuration.GetSection("domainSettings"));
            services.AddHttpClient();
            services.AddHttpContextAccessor();

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true; // set Modelstate validation manual
            });


            services.AddApplication(Configuration);
            services.AddCore();
            services.AddDataAccess();
            services.AddData(Configuration);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthorization();
            app.UseAuthentication();




            app.UseMiddleware<LoggingMiddleware>();

            app.UseApiResponseAndExceptionWrapper(new AutoWrapperOptions
            {
                //UseApiProblemDetailsException = true,
                IgnoreNullValue = false,
                ShowStatusCode = true,
                ShowIsErrorFlagForSuccessfulResponse = true,
                //IgnoreWrapForOkRequests = true,
                EnableExceptionLogging = true,
                EnableResponseLogging = false,
                LogRequestDataOnException = false,
                ShouldLogRequestData = false,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                UseCustomExceptionFormat = false,
            });
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        }
    }
}

﻿using Domain.Common.Configurations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class WordTag : Entity
    {
        [ForeignKey("Tag")]
        public int TagId { get; set; }
        [ForeignKey("Word")]
        public int WordId { get; set; }
        public Tag Tag { get; set; }
        public Word Word { get; set; }
    }
}

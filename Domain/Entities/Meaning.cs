﻿using Domain.Common.Configurations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Meaning : Entity
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }

        [ForeignKey("Word")]
        public int WordId { get; set; }
        public Word Word { get; set; }
    }
}

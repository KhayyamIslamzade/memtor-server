﻿using Domain.Common.Configurations;
using Microsoft.AspNetCore.Identity;
using System;
using Core.Enums;

namespace Domain.Entities.Identity
{
    public class UserLogin : IdentityUserLogin<string>, IEntity
    {
        public RecordStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}

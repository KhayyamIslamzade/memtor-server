﻿using Domain.Common.Configurations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class GroupWord : Entity
    {
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        [ForeignKey("Word")]
        public int WordId { get; set; }
        public Group Group { get; set; }
        public Word Word { get; set; }
    }
}

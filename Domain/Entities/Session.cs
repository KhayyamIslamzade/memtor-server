﻿using Core.Enums;
using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Session : Entity, IAgregate
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public GameLevelEnum GameLevel { get; set; } = GameLevelEnum.Easy;
        public GameTypeEnum GameType { get; set; } = GameTypeEnum.MeaningGame;
        public SessionStatusEnum SessionStatus { get; set; } = SessionStatusEnum.Pending;

        [ForeignKey("Group")]
        public int? GroupId { get; set; }
        public Group Group { get; set; }
        [ForeignKey("Owner")]
        [Required]
        public string OwnerId { get; set; }
        public User Owner { get; set; }
        public ICollection<Game> Games { get; set; } = new Collection<Game>();

    }
}

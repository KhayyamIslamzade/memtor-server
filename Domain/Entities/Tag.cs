﻿using Core.Constants;
using Domain.Common.Configurations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Tag : Entity, IAgregate
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(StringLengthConstants.LengthXxxs)]
        public string Title { get; set; }
        //public ICollection<WordTag> Words { get; set; } = new Collection<WordTag>();

        [ForeignKey("Word")]
        public int WordId { get; set; }
        public Word Word { get; set; }

    }
}

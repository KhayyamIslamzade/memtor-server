﻿using Core.Enums;
using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Game : Entity, IAgregate
    {
        [Key]
        public int Id { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsTrueAnswer { get; set; } = false;

        public int RetryCount { get; set; }
        public GameLevelEnum GameLevel { get; set; } = GameLevelEnum.Easy;
        public GameTypeEnum GameType { get; set; } = GameTypeEnum.MeaningGame;
        public AnswerStatusEnum AnswerStatus { get; set; } = AnswerStatusEnum.Unanswered;

        [ForeignKey("Word")]
        public int WordId { get; set; }
        public Word Word { get; set; }
        [ForeignKey("Owner")]
        [Required]
        public string OwnerId { get; set; }
        public User Owner { get; set; }
        public ICollection<Answer> Answers { get; set; } = new Collection<Answer>();

    }
}

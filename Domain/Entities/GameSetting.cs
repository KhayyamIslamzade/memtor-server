﻿using Core.Enums;
using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class GameSetting : Entity
    {
        [Key]
        [Required]
        [ForeignKey("Layer")]
        public string UserId { get; set; }
        public User User { get; set; }

        public GameLevelEnum GameLevel { get; set; } = GameLevelEnum.Easy;
        public GameTypeEnum GameType { get; set; } = GameTypeEnum.MeaningGame;


    }
}

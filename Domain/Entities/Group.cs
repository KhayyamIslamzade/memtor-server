﻿using Core.Constants;
using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Group : Entity, IAgregate
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(StringLengthConstants.LengthMd)]
        public string Title { get; set; }
        public bool IsPublic { get; set; } = false;
        [ForeignKey("Owner")]
        public string OwnerId { get; set; }
        public User Owner { get; set; }


        public ICollection<GroupWord> Words { get; set; } = new Collection<GroupWord>();
    }
}

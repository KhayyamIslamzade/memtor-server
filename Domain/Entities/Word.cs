﻿using Core.Constants;
using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Word : Entity, IAgregate
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        [StringLength(StringLengthConstants.LengthXs)]
        public string Title { get; set; }
        [StringLength(StringLengthConstants.LengthLg)]
        public string Description { get; set; }

        public ICollection<Meaning> Meanings { get; set; } = new Collection<Meaning>();
        //public ICollection<GroupWord> Groups { get; set; } = new Collection<GroupWord>();
        //public ICollection<WordTag> Tags { get; set; } = new Collection<WordTag>();
        public ICollection<Tag> Tags { get; set; } = new Collection<Tag>();
        public ICollection<Game> Games { get; set; } = new Collection<Game>();

        [ForeignKey("Owner")]
        [Required]
        public string OwnerId { get; set; }
        public User Owner { get; set; }

    }
}

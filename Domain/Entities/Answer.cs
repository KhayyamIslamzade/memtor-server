﻿using Domain.Common.Configurations;
using Domain.Entities.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Answer : Entity
    {
        [Key]
        public int Id { get; set; }
        //answer
        public string Label { get; set; }
        public bool IsTrue { get; set; }

        [ForeignKey("Game")]
        public int GameId { get; set; }
        public Game Game { get; set; }

        [ForeignKey("Owner")]
        [Required]
        public string OwnerId { get; set; }
        public User Owner { get; set; }
    }
}

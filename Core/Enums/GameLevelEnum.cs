﻿

namespace Core.Enums
{
    public enum GameLevelEnum
    {
        Easy = 1,
        Medium,
        Hard,
        Random
    }
}

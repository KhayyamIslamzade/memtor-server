﻿

namespace Core.Enums
{
    public enum PermissionEnum
    {
        Add,
        Edit,
        List,
        Delete,


        //directive
        Admin,
        Moderator
    }
}

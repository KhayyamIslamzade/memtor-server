﻿

namespace Core.Enums
{
    public enum SessionStatusEnum
    {
        Pending = 1,
        Started,
        Closed,
    }
}

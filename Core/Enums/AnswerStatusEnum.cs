﻿

namespace Core.Enums
{
    public enum AnswerStatusEnum
    {
        Answered = 1,
        Unanswered,
        Passed
    }
}

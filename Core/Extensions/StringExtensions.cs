﻿using Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Extensions
{
    public static class StringExtensions
    {
        public static bool IsEmail(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;
            var emailRegex = RegexConstants.EmailRegex;
            var re = new Regex(emailRegex);
            return re.IsMatch(s);

        }

        public static bool IsUsername(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;
            var usernameRegex = RegexConstants.UserNameRegex;
            var re = new Regex(usernameRegex);
            return re.IsMatch(s);
        }
        public static string FirstLetterToUpper(this string str)
        {


            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public static string ReplaceRandomChars(this string s, char c, int count = 1)
        {
            var content = s;
            var length = content.Length;

            if (count > length)
                throw new Exception("Replaced char count cant be higher than string length!");
            var replacedIndexes = new List<int>();
            for (int i = 0; i < count; i++)
            {
                int randomIndex = 0;
                bool isWhiteSpace = true;

                do
                {
                    randomIndex = new Random().Next(0, length - 1);
                    isWhiteSpace = Char.IsWhiteSpace(content[randomIndex]);
                    Console.WriteLine("Char :{0}   Result:{1}", content[randomIndex], isWhiteSpace);

                } while (replacedIndexes.Contains(randomIndex) || isWhiteSpace);

                StringBuilder sb = new StringBuilder(content);
                sb[randomIndex] = c;
                content = sb.ToString();
                replacedIndexes.Add(randomIndex);


            }

            return content;
        }
    }
}
